#include <stdlib.h>
#include <stdio.h>
#include "chess.h"
#include <string.h>

//AVANCE FUNCIONES DE CHESS

char invColor(char c){
  switch( c ){
  case '_': return '=';
  case '=': return '_';
  case '.': return '@';
  case '@': return '.';
  case ' ': return ' ';
  }

  return '#';
}

char** reverse(char** c){	

  int filas=58;
  int columnas=58;
  int i,j;

  for(i=0;i<filas;i++){
    char* aux = *(c+i);
    char* aux2 = (char*) malloc(sizeof(char)*columnas);
    for(j=0;j<columnas;j++){
      aux2[j] = invColor(*(aux+j));
    }
  c[i] = aux2;
  }

  return c;	
}

int countRow(char** a){
  int count=0;
  while(a[count]){
  count++;
  }

  return count;
}
int countCol(char** a){
  int count=0;
  while(a[0][count]){
  count++;
  }

  return count;
}


char** join(char** a, char** b){

  int c1,c2,f,i,j;
  c1=countCol(a);
  c2=countCol(b);
  f=countRow(a);
  char** esp;
  char** rsp;
  int AllCol = c1+c2;
  esp=(char**)malloc(sizeof(char*)*(f+1));
  for(i=0;i<f;i++){
  esp[i]=(char*)malloc(sizeof(char)*(AllCol+1));
  }
  rsp=esp;
  for(i=0;i<f;i++){
    for(j=0;j<AllCol;j++){
      rsp[i][j]=a[i][j];
      if(j>=c1){
        rsp[i][j]=b[i][j-c1];
      }
    }
    rsp[i][AllCol]=0;
  }
  rsp[f]=0;
  return rsp;
} 

char** superImpose(char** c,char** c2){
  int filas=58;
  int columnas=58;
  int i,j,k;

  for(i=0;i<filas;i++){
    char* aux = *(c+i);
    char* aux2 = *(c2+i);
    char* aux3 = (char*) malloc(sizeof(char)*columnas);

    for(j=0;j<columnas;j++){
      aux3[j] = *(aux+j);
    }

    for(k=0;k<columnas;k++){
      if(aux3[k]==' '){
        aux3[k] = *(aux2+k);
      }		
    }
  c[i]=aux3;
  }

  return c;
}

char** rotateL(char** a){
  
  int k;
  int i;
  int j;

  char** cuadrado = (char**)malloc(sizeof (char *)* 58);
  for (k = 0; k < 58; k++)
    cuadrado[k] = (char*)malloc(sizeof(char) * 58);
  for(i = 0; i < 58; i++){
    char* line1 = *(a + i);
    
    for(j = 0; j < 58; j++){
      cuadrado[i][j] = a[j][57-i];
    }
  }

  return cuadrado;
}

char** rotateR(char** a){
  
  int k;
  int i;
  int j;

  char** cuadrado = (char**)malloc(sizeof (char *)* 58);
  for (k = 0; k < 58; k++)
    cuadrado[k] = (char*)malloc(sizeof(char) * 58);
  for(i = 0; i < 58; i++){
    char* line1 = *(a + i);
    
    for(j = 0; j < 58; j++){
      cuadrado[i][j] = a[57-j][i];
    }
  }

  return cuadrado;
}